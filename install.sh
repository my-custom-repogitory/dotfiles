#!/bin/bash

set -u

GIT_DOT="https://github.com/Kazanami/dotfiles.git";
DOT_DIRECTORY="${HOME}/dotfiles";
GIT_PURE="https://github.com/chris-marsh/pureline.git"
if [ -d ${DOT_DIRECTORY} ];then
	INSTALL_PACK_LIST="./install_pack_list.txt"
else
	INSTALL_PACK_LIST="https://raw.githubusercontent.com/Kazanami/dotfiles/master/install_pack_list.txt"
fi

# memo
#
#-> Vim
#-> git
#-> axel 
#-> curl
#-> sshd
#-> npm
#-> node
#-> make
#-> automake
#-> gcc                                                   

require_install=(`wget -q -O - ${INSTALL_PACK_LIST} | cat | xargs `);

install_pack() {
	echo "";
	echo "Check Installed Package"
	for i in "${require_install[@]}";
	do
		if [ `which ${i}` ];
		then
			echo "${i} Installed";
		else
			echo "Not Installed ${i}.";
			install_list+="${i} ";
		fi
	done
	echo "Install List: ${install_list}";
	sudo apt update;
	sudo apt -y install ${install_list};
}

download_dotfiles(){
	sleep 5s
	echo "Downloading My Dotfiles...";
	if [ `which git` ];then
		git clone ${GIT_DOT} ${DOT_DIRECTORY};
	else 
		echo "Git not installed!!";
		exit 3;
	fi
}



download_pureline(){
	echo "PURELINE PROMPT INSTALLING..."
	git clone ${GIT_PURE} ${HOME}/.local/pureline
	ln -svfn ~/.local/pureline/configs/powerline_full_256col.conf ~/.pureline.conf
	if [ -d ~/.local/bin ];then
		echo "Skip ~/.local/bin";
	else
		echo "create ~/.local/bin";
		mkdir -p ~/.local/bin
	fi
	ln -svfn ~/.local/pureline/pureline ~/.local/bin/pureline;
}


download_fonts(){
	echo "Downloading DejaVu Sans Mono for Powerline.ttf Font...";
	if [ -d "~/.local/fonts" ];
	then
		echo "skip ~/.local/share/fonts";
	else
		mkdir -p ~/.local/share/fonts
	fi

	wget -O "DejaVu Sans Mono for Powerline.ttf" "https://github.com/powerline/fonts/blob/master/DejaVuSansMono/DejaVu%20Sans%20Mono%20for%20Powerline.ttf?raw=true"
	mv "${HOME}/DejaVu Sans Mono for Powerline.ttf" "${HOME}/.local/share/fonts/"
	if [ -f "${HOME}/.local/share/fonts/DejaVu Sans Mono for Powerline.ttf" ];
	then
		echo "Complited";
	fi
}

install_terminal_config(){
	echo "installing Terminal Config..."
	cat ${DOT_DIRECTORY}/powerline-fonts.dconf | dconf load '/org/gnome/terminal/legacy/'
}

wget -q -O - "https://raw.githubusercontent.com/Kazanami/dotfiles/master/Kazanami_dotfiles_installer.txt" 
echo "MIT LICENSE 2019 Kazanami"


install_pack;
download_dotfiles;
download_pureline;
download_fonts;
install_terminal_config;

#wget -q -O - "https://raw.githubusercontent.com/Kazanami/dotfiles/master/link.sh" | bash 
bash "${DOT_DIRECTORY}/link.sh"
