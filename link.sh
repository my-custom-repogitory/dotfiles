#!/bin/bash
dotfiles_logo='
      | |     | |  / _(_) |           
    __| | ___ | |_| |_ _| | ___  ___  
   / _` |/ _ \| __|  _| | |/ _ \/ __| 
  | (_| | (_) | |_| | | | |  __/\__ \ 
   \__,_|\___/ \__|_| |_|_|\___||___/ 

  Copyright (c) 2019 Kazanami
  Licensed under the MIT license.
'
echo "";
echo "${dotfiles_logo}";

echo "DOTFILES SETUP START!";

cd ~/dotfiles

for f in .?*; do
	[ "$f" = ".git" ] && continue;
	[ "$f" = ".gitignore" ] && continue;
	[ "$f" = '..' ] && continue;
	ln -snfv ~/dotfiles/"$f" ~/"${f}"
done

echo "DOTFILES SETUP FINISHED!";
